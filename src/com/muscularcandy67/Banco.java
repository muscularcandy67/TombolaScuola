package com.muscularcandy67;

import java.util.Random;

public class Banco {

    private int[] numeri;
    private int current;

    final private boolean[] estratti;

    public Banco() {
        estratti = new boolean[90];

        numeri = new int[90];
        for (int i=0; i<90; i++)
            numeri[i] = i+1;
        shuffle(numeri);

        current=0;
    }

    public int estraiNumero() {
        int n = numeri[current];
        current++;
        estratti[n-1] = true;
        return n;
    }

    public void stampa() {
        for (int i=0; i<9; i++) {
            StringBuilder output = new StringBuilder();
            for (int j=1; j<10; j++) {
                int n = i*10+j;
                boolean es = estratti[n];
                output.append(String.format("%c%02d%c", (es ? '[' : ' '), n, (es ? '[' : ' ')));
            }
            info(output.toString());
        }
    }

    private static void shuffle(int[] array){
        int index, temp;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
    }

    private static void info(final String s) {
        String[] lines = s.split(System.getProperty("line.separator"));
        for (String l: lines)
            System.out.println(">>> "+l);
    }
}
