package com.muscularcandy67;

public class Giocatore {
    private final String nome;
    private int[] numeri; // numeri contenuti in questa cartella
    final private boolean[] segnati; // true se il numero corrispondente è segnato

    public Giocatore (String nome) {
        segnati = new boolean[15];
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public String toString() {
        return nome;

    }

    private static int generaCasuale(){
        return 1 +(int)(Math.random()*(90 - 1));
    }

    private void inizializzaCartella() {
        numeri = new int[15];
        for (int i=0; i<15; i++) {
            final int n = generaCasuale();
            if (indexOf(n, numeri, i) >= 0) {
                i--;
            } else {
                numeri[i] = n;
            }
        }
    }

    private static int indexOf(int num, int[] array, int dim){
        for(int i=0; i<dim; i++){
            if(num==array[i]){
                return i;
            }
        }
        return -1;
    }
}
